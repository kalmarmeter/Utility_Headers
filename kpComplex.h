#ifndef KPCOMPLEX_H_INCLUDED
#define KPCOMPLEX_H_INCLUDED

#include "kpVector.h"

struct kpComplex {
private:
    kpComplex(double _re, double _im): _c(kpVector::make_from_xy(_re,_im)) {}
    kpComplex(const kpVector& _v): _c(_v) {}
public:
    kpVector _c;

    static kpComplex make_from_reim(double _re, double _im) {
        return kpComplex(_re,_im);
    }

    static kpComplex make_from_vector(const kpVector& _v) {
        return kpComplex(_v);
    }

    friend std::ostream& operator<< (std::ostream& _os, const kpComplex& _c) {
        _os << _c._c;
        return _os;
    }

    kpVector vector() const {
        return _c;
    }

    double real() const {
        return vector().x();
    }

    double imag() const {
        return vector().y();
    }

    double abs() const {
        return vector().length();
    }

    double arg() const {
        return vector().angle();
    }

    friend kpComplex operator+ (const kpComplex& _c1, const kpComplex& _c2) {
        return kpComplex::make_from_vector(_c1.vector() + _c2.vector());
    }

    friend kpComplex operator* (const kpComplex& _c1, const kpComplex& _c2) {
        return kpComplex::make_from_reim(_c1.real()*_c2.real() - _c1.imag()*_c2.imag(), _c1.real()*_c2.imag() + _c1.imag()*_c2.real());
    }

    bool operator== (const kpComplex& _c) const {
        return (vector() == _c.vector());
    }
};

#endif // KPCOMPLEX_H_INCLUDED
