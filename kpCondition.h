#ifndef KPFIND_H_INCLUDED
#define KPFIND_H_INCLUDED

#include <vector>

namespace kpCondition {

template <typename T>
static auto find_first (T& coll, auto cond) {
    for (auto it = coll.begin(); it != coll.end(); ++it) {
        if (cond(*it)) {
            return &(*it);
        }
    }
    return nullptr;
}

template <typename T>
static auto find_all (T& coll, auto cond) {
    std::vector<typename T::value_type*> res;
    for (auto it = coll.begin(); it != coll.end(); ++it) {
        if (cond(*it)) {
            res.push_back(&(*it)); //CONVERTING ITERATOR TO POINTER
        }
    }
    return res;
}

template <typename T>
struct equals {
private:
    T e;
public:
    equals(T _e) : e(_e) {}
    bool operator() (T _a) {
        return (e == _a);
    }
};

template <typename T>
struct greaterThan {
private:
    T e;
public:
    greaterThan(T _e) : e(_e) {}
    bool operator() (T _a) {
        return (e < _a);
    }
};

template <typename T>
struct lessThan {
private:
    T e;
public:
    lessThan(T _e) : e(_e) {}
    bool operator() (T _a) {
        return (e < _a);
    }
};

// ONLY FOR INTEGERS

struct remainder {
private:
    int m,r;
public:
    remainder(int _m, int _r) : m(_m), r(_r) {}
    bool operator() (int a) {
        return (a % m == r);
    }
};

}

#endif // KPFIND_H_INCLUDED
