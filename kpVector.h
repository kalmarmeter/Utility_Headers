#ifndef KPVECTOR_H_INCLUDED
#define KPVECTOR_H_INCLUDED

#include "kpPoint.h"
#include <cmath>

#define PI 3.141592

struct kpVector {
private:
    kpVector(double _x, double _y) : p(_x,_y) {}
public:
    kpPoint p;

    static kpVector make_from_xy(double _x, double _y) {
        return kpVector(_x, _y);
    }

    static kpVector make_from_point(const kpPoint& _p) {
        return kpVector(_p.x, _p.y);
    }

    static kpVector make_from_two_xy(double _x1, double _y1, double _x2, double _y2) {
        return kpVector(_x2-_x1, _y2-_y1);
    }

    static kpVector make_from_two_points(const kpPoint& _p1, const kpPoint& _p2) {
        return kpVector(_p2.x-_p1.x, _p2.y-_p1.y);
    }

    double x() const {
        return p.x;
    }

    double y() const {
        return p.y;
    }

    kpPoint point() const {
        return p;
    }

    double length() const {
        return (sqrt(p.x*p.x + p.y*p.y));
    }

    double angle() const {
        if (p.x > 0) {
            return atan(p.y / p.x);
        } else {
            return (atan(p.y / p.x) + PI);
        }
    }

    friend kpVector operator+ (const kpVector& _v1, const kpVector& _v2) {
        return kpVector(_v1.x()+_v2.x(),_v1.y()+_v2.y());
    }

    bool operator== (const kpVector& _v) const {
        return (p == _v.p);
    }

    friend std::ostream& operator<< (std::ostream& _os, const kpVector& _v) {
        _os << _v.p << " " << _v.length() << " " << _v.angle();
        return _os;
    }

};

#endif // KPVEC_H_INCLUDED
