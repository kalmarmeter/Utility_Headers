#ifndef KPPOINT_H_INCLUDED
#define KPPOINT_H_INCLUDED

#include <iostream>

struct kpPoint {
private:
    kpPoint(double _x, double _y) : x(_x), y(_y) {}
public:
    double x,y;

    static kpPoint make_from_xy(double _x, double _y) {
        return kpPoint(_x,_y);
    }

    static kpPoint make_from_point(const kpPoint& _p) {
        return kpPoint(_p.x,_p.y);
    }

    bool operator== (kpPoint _p) const {
        return (x == _p.x && y == _p.y);
    }

    friend std::ostream& operator<< (std::ostream& _os, const kpPoint& _p) {
        _os << _p.x << " " << _p.y;
        return _os;
    }

    friend class kpVector;
};

#endif // POINT_H_INCLUDED
