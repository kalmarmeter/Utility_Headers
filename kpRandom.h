#ifndef KPRANDOM_H_INCLUDED
#define KPRANDOM_H_INCLUDED

#include <ctime>
#include <cstdlib>

struct kpRandom {
    static void randomize(unsigned int seed = time(0)) {
        srand(seed);
    }
    static int random() {
        return rand();
    }
    static int random_range(int lower, int upper) {
        if (lower > upper) {
            return random();
        }
        return (random() % (upper-lower+1) + lower);
    }
};

#endif // KPRANDOM_H_INCLUDED
